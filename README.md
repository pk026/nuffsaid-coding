# Nuffsaid Coding

This Repo have two programm files and one data source file.
1. count_schools.py
    1.  Reads data file
    2.  Process data
    3.  Prints stats
    # How to run
    1. Go to python shell
    2. import print_counts
    3. call print_counts
    4. or you may run it directly using command python count_schools.py
2. school_search.py
    1.  Reads data file 
    2.  Builds index
    3.  Search
    # How to run
    1. Go to python shell
    2. import search_schools
    3. call search_schools

