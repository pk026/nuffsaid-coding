# Auther Pramod
import csv
import time
import itertools


class ProcessSchoolData:
    """
        This class process a file school_data.csv
        which is supposed to be in same dir as this file is
    """

    CSV_FILE_PATH = "school_data.csv"   # put your own path if you have csv at different location
    SCHOOL_COUNT = 0
    SCHOOL_COUNT_IN_STATE = dict()
    SCHOOL_COUNT_IN_METRO = dict()
    SCHOOL_COUNT_IN_CITY = dict()
    CITY_HAVING_SCHOOL = set()

    def __init__(self):
        self.read_file()

    def read_file(self):
        # csv file we are going read could be very large to lead in RAM at once
        # We are going to use islice to avoid this loading
        NO_OF_LINES = 10
        with open(self.CSV_FILE_PATH, 'r') as fp:
            while True:
                lines = itertools.islice(fp, NO_OF_LINES)
                csv_reader = csv.reader(lines, delimiter=',')
                line_count = 0
                for row in csv_reader:
                    line_count += 1
                    # skip the header and move to next line
                    if row[0] == 'NCESSCH':
                        continue
                    self.build_data_set(row)
                if line_count < 10:
                    # we completed reading the file lets break the infinite loop
                    break

    def build_data_set(self, row):
        self.SCHOOL_COUNT += 1
        NCESSCH, LEAID, LEANM05, SCHNAM05, LCITY05, LSTATE05,\
            LATCOD, LONCOD, MLOCALE, ULOCALE, status05 = row
        if LSTATE05 in self.SCHOOL_COUNT_IN_STATE:
            self.SCHOOL_COUNT_IN_STATE[LSTATE05] += 1
        else:
            self.SCHOOL_COUNT_IN_STATE[LSTATE05] = 1

        if MLOCALE in self.SCHOOL_COUNT_IN_METRO:
            self.SCHOOL_COUNT_IN_METRO[MLOCALE] += 1
        else:
            self.SCHOOL_COUNT_IN_METRO[MLOCALE] = 1
        if LCITY05 in self.SCHOOL_COUNT_IN_CITY:
            self.SCHOOL_COUNT_IN_CITY[LCITY05] += 1
        else:
            self.SCHOOL_COUNT_IN_CITY[LCITY05] = 1

        self.CITY_HAVING_SCHOOL.add(LCITY05)

    def get_total_schools(self):
        return self.SCHOOL_COUNT

    def get_schools_by_state(self):
        school_count_str = ""
        for state, school_count in self.SCHOOL_COUNT_IN_STATE.items():
            school_count_str += "{}: {}\n".format(state, school_count)
        return school_count_str

    def get_schools_by_metro(self):
        school_count_str = ""
        for metro, school_count in self.SCHOOL_COUNT_IN_METRO.items():
            school_count_str += "{}: {}\n".format(metro, school_count)
        return school_count_str

    def get_city_most_schools(self):
        max_city = ''
        max_city_cnt = 0
        for city, cnt in self.SCHOOL_COUNT_IN_CITY.items():
            if cnt > max_city_cnt:
                max_city_cnt = cnt
                max_city = city

        school_count_str = "{} ({} schools)".format(max_city, max_city_cnt)
        return school_count_str

    def get_city_min_one_school(self):
        return len(self.CITY_HAVING_SCHOOL)

def print_counts():
    psd = ProcessSchoolData()
    print("Total Schools: {}\n".format(psd.get_total_schools()))
    print("Schools by State:\n {}".format(psd.get_schools_by_state()))
    print("Schools by Metro-centric locale:\n {}".format(psd.get_schools_by_metro()))
    print("City with most schools: {}".format(psd.get_city_most_schools()))
    print("Unique cities with at least one school: {}".format(psd.get_city_min_one_school()))

if __name__ == "__main__":
    print_counts()
