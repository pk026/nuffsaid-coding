import csv
import time
import itertools


class School:

    def __init__(self, name, city, state):
        self.name = name
        self.city = city
        self.state = state

    def __str__(self):
        return "{} \n{}, {}".format(self.name, self.city, self.state)


class ProcessData:

    CSV_FILE_PATH = "school_data.csv"
    KEY_TO_IDS = dict()
    IDS_TO_SCHOOL = dict()

    def __init__(self):
        self.read_file()

    def read_file(self):
        # csv file we are going read could be very large to lead in RAM at once
        # We are going to use islice to avoid this loading
        NO_OF_LINES = 10
        with open(self.CSV_FILE_PATH, 'r') as fp:
            while True:
                lines = itertools.islice(fp, NO_OF_LINES)
                csv_reader = csv.reader(lines, delimiter=',')
                line_count = 0
                for row in csv_reader:
                    line_count += 1
                    # skip the header and move to next line
                    if row[0] == 'NCESSCH':
                        continue
                    self.build_token(row)
                if line_count < 10:
                    # we completed reading the file lets break the infinite loop
                    break

    def clean_words(self, school_with_address):
        cleand_words = []
        for word in school_with_address.split():
            cleand_words.append(word.lower())
        return cleand_words

    def build_token(self, row):
        NCESSCH, LEAID, LEANM05, SCHNAM05, LCITY05, LSTATE05,\
            LATCOD, LONCOD, MLOCALE, ULOCALE, status05 = row
        self.IDS_TO_SCHOOL[NCESSCH] = School(SCHNAM05, LCITY05, LSTATE05)
        school_with_address = "{} {} {}".format(
            SCHNAM05, LCITY05, LSTATE05
        )

        for word in self.clean_words(school_with_address):
            if word not in self.KEY_TO_IDS:
                self.KEY_TO_IDS[word] = set([NCESSCH])
            else:
                self.KEY_TO_IDS[word].add(NCESSCH)

    def search(self, query):
        KEY_SCORE = dict()
        for word in self.clean_words(query):
            for key in self.KEY_TO_IDS.get(word, []):
                if key in KEY_SCORE:
                    KEY_SCORE[key] += 1
                else:
                    KEY_SCORE[key] = 1
        KEY_SCORE = sorted(KEY_SCORE.items(), key=lambda x: x[1], reverse=True)
        school_ids = []
        for key_value in KEY_SCORE:
            school_ids.append(key_value[0])
            if len(school_ids) >= 3:
                break
        return school_ids

def search_schools(query):
    pd = ProcessData()
    start_time = time.time()
    school_ids = pd.search(query=query)
    print("Results for {} (search took: {}s)".format(
        query, time.time()-start_time)
    )
    for count, school_id in enumerate(school_ids):
        school = pd.IDS_TO_SCHOOL[school_id]
        print("{} {}".format(count + 1, school))

# if __name__ == "__main__":
    # query = "elementary school highland park"
    # search_schools(pd, query)
    # query = "jefferson belleville"
    # search_schools(pd, query)
    # query = "riverside school 44"
    # search_schools(pd, query)
    # query = "granada charter school"
    # search_schools(pd, query)
    # query = "foley high alabama"
    # search_schools(pd, query)
    # query = "KUSKOKWIM"
    # search_schools(pd, query)
